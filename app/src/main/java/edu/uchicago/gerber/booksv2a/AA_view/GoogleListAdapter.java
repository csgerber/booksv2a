package edu.uchicago.gerber.booksv2a.AA_view;


import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import edu.uchicago.gerber.booksv2a.CC_model.models.Item;
import edu.uchicago.gerber.booksv2a.R;

public class GoogleListAdapter extends RecyclerView.Adapter<GoogleListAdapter.GoogleSearchResultHolder> {

    //the model for this adapter
    private List<Item> items = new ArrayList<>();

    //we need this for the click - this will be the calling Fragment
    AdapterCallback adapterCallback;

    //pass in the Fragment which IS an AdapterCallback
    public GoogleListAdapter(AdapterCallback adapterCallback) {
        this.adapterCallback = adapterCallback;
    }

    @NonNull
    @Override
    public GoogleSearchResultHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.google_item, parent, false);

        return new GoogleSearchResultHolder(itemView);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull GoogleSearchResultHolder holder, int position) {
        Item volume = items.get(position);

        holder.titleTextView.setText(volume.getVolumeInfo().getTitle());
        holder.publishedDateTextView.setText(volume.getVolumeInfo().getPublishedDate());

        if (volume.getVolumeInfo().getImageLinks() != null) {
            String imageUrl = volume.getVolumeInfo().getImageLinks().getSmallThumbnail()
                    .replace("http://", "https://");

            Glide.with(holder.itemView)
                    .load(imageUrl)
                    .into(holder.smallThumbnailImageView);
        }

        if (volume.getVolumeInfo().getAuthors() != null) {
            String authors = String.join(", ", volume.getVolumeInfo().getAuthors());
            holder.authorsTextView.setText(authors);
        }

        holder.cardBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (adapterCallback != null) {
                        adapterCallback.onBookClick(items,position);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (items == null){
            return 0;
        }
        return items.size();
    }

    public void setItems(List<Item> items) {

        this.items = items;
        notifyDataSetChanged();
    }


    static class GoogleSearchResultHolder extends RecyclerView.ViewHolder {

        private final TextView titleTextView;
        private final TextView authorsTextView;
        private final TextView publishedDateTextView;
        private final ImageView smallThumbnailImageView;
        private final CardView cardBook;

        public GoogleSearchResultHolder(@NonNull View itemView) {
            super(itemView);

            titleTextView = itemView.findViewById(R.id.book_item_title);
            authorsTextView = itemView.findViewById(R.id.book_item_authors);
            publishedDateTextView = itemView.findViewById(R.id.book_item_publishedDate);
            smallThumbnailImageView = itemView.findViewById(R.id.book_item_smallThumbnail);
            cardBook = itemView.findViewById(R.id.card_book);
        }
    }
    //the adapter callback will be the Fragment
    public interface AdapterCallback {
        void onBookClick(List<Item> volumes,int position);

    }
}