package edu.uchicago.gerber.booksv2a.CC_model.service;

import edu.uchicago.gerber.booksv2a.CC_model.models.GoogleResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface BookService {

    @GET("/books/v1/volumes")
    Call<GoogleResponse> searchVolumes(
            @Query("q") String query,
            @Query("maxResults") String maxResults
    );
}