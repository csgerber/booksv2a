package edu.uchicago.gerber.booksv2a.AA_view;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import edu.uchicago.gerber.booksv2a.BB_viewmodel.GoogleViewModel;
import edu.uchicago.gerber.booksv2a.CC_model.models.GoogleResponse;
import edu.uchicago.gerber.booksv2a.CC_model.models.Item;
import edu.uchicago.gerber.booksv2a.CC_model.repo.BookRepository;
import edu.uchicago.gerber.booksv2a.R;

/**
 * A fragment representing a list of Items.
 */
public class GoogleFragment extends Fragment implements GoogleListAdapter.AdapterCallback {

    private EditText editQuery;
    private ProgressBar progressBar;


    private GoogleViewModel viewModel;
    private GoogleListAdapter adapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        adapter = new GoogleListAdapter(this);
        viewModel = ViewModelProviders.of(this).get(GoogleViewModel.class);

        viewModel.getGoogleResponseMutableLiveData().observe(getViewLifecycleOwner(), new Observer<GoogleResponse>() {
            @Override
            public void onChanged(GoogleResponse volumesResponse) {
                if (volumesResponse != null) {
                    adapter.setItems(volumesResponse.getItems());
                    progressBar.setVisibility(View.INVISIBLE);
                }
            }
        });


        View containerView = inflater.inflate(R.layout.fragment_google, container, false);

        editQuery = containerView.findViewById(R.id.editQuery);
        editQuery.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {

                if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    MainActivity.hideKeyboardFrom(getContext(), GoogleFragment.this.getView());
                    viewModel.searchVolumes(editQuery.getText().toString());
                    progressBar.setVisibility(View.VISIBLE);

                }
                return true;
            }
        });

        progressBar = containerView.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

        RecyclerView recyclerView = containerView.findViewById(R.id.recyclerBooks);
        recyclerView.setAdapter(adapter);

        return containerView;
    }

    @Override
    public void onBookClick(List<Item> items, int position) {

        try {
            String auth = items.get(position).getVolumeInfo().getAuthors().get(0);
            String title = items.get(position).getVolumeInfo().getTitle();
            String year = items.get(position).getVolumeInfo().getPublishedDate();
            String imageUrlLarge = items.get(position).getVolumeInfo().getImageLinks().getThumbnail();
            String description = items.get(position).getVolumeInfo().getDescription();

            Bundle bundle = new Bundle();
            bundle.putString(getString(R.string.auth), auth);
            bundle.putString(getString(R.string.title), title);
            bundle.putString(getString(R.string.year), year);
            bundle.putString(getString(R.string.imageUrlLarge), imageUrlLarge);
            bundle.putString(getString(R.string.description), description);

            Navigation.findNavController(getView()).navigate(R.id.action_googleFragment_to_detailFragment, bundle);

        } catch (Exception e) {
            e.printStackTrace();
            Snackbar.make(getView(), "Insufficient details", Snackbar.LENGTH_LONG)
                    .setAction("CLOSE", null)
                    .setActionTextColor(ContextCompat.getColor(getActivity(), android.R.color.holo_red_light))
                    .show();
        }

    }
}